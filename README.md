# Flutter Movies

App que muestra las últimas películas en cartelera y un resumen de ellas.

Al descargar el repositorio, se debe copiar el archivo **.env.example** a **.env** y llenar los datos de configuración con los datos pertinentes.

```bash
cp .env.example .env
```

---

Copiar el archivo y actualizar los datos de configuración.
```bash
cd android/
cp key.properties.example key.properties
```

![Screenshot](screenshot.png "Screenshot")