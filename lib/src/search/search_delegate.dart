import 'package:flutter/material.dart';
import 'package:movies/src/models/movie.dart';
import 'package:movies/src/providers/movies_providers.dart';

class DataSearch extends SearchDelegate {
  /// Las acciones o botones del appbar.
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  /// El icono a la izquierda del appbar.
  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  /// Crear los resultados que se muestran.
  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  /// Crear las sugerencias que aparecen cuando la persona escribe.
  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) return Container();

    final moviesProvider = MoviesProvider();

    return FutureBuilder(
      future: moviesProvider.searchMovies(query),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              Movie movie = snapshot.data[index];

              return ListTile(
                leading: movie.posterPath != null
                    ? _MovieImage(movie: movie)
                    : _ImageNotFound(),
                title: Text(movie.title),
                subtitle: Text(movie.originalTitle),
                onTap: () {
                  close(context, null);
                  movie.uniqueId = '${movie.id}-search';
                  Navigator.of(context).pushNamed('detail', arguments: movie);
                },
              );
            },
          );
        }

        return Container(
          padding: EdgeInsets.all(20.0),
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}

class _ImageNotFound extends StatelessWidget {
  const _ImageNotFound({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(
      image: AssetImage('assets/images/no-image-found.png'),
      fit: BoxFit.cover,
      height: 50.0,
      width: 50.0,
    );
  }
}

class _MovieImage extends StatelessWidget {
  const _MovieImage({
    Key key,
    @required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      image: NetworkImage(movie.getPosterImage()),
      placeholder: AssetImage('assets/images/jar-loading.gif'),
      fit: BoxFit.cover,
      height: 50.0,
      width: 50.0,
    );
  }
}
