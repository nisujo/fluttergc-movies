import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:movies/src/models/actor.dart';
import 'package:movies/src/models/movie.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as dotenv;

class MoviesProvider {
  final String _apiKey = dotenv.env['API_KEY'];
  final String _url = dotenv.env['API_URL'];
  final String _language = dotenv.env['API_LANGUAGE'];
  int _popularPage = 0;
  int _nowPage = 0;
  bool _loading = false;

  List<Movie> _popular = [];

  final _popularStreamController = StreamController<List<Movie>>.broadcast();

  Function(List<Movie>) get popularSink => _popularStreamController.sink.add;

  Stream<List<Movie>> get popularStream => _popularStreamController.stream;

  void disposeStreams() {
    _popularStreamController?.close();
  }

  Future<List<Movie>> _processResponse(Uri urlParam) async {
    final response = await http.get(urlParam);
    final decodedResponse = json.decode(response.body);
    final movies = Movies.fromJsonList(decodedResponse['results']);

    return movies.items;
  }

  Future<List<Movie>> getMoviesNowPlaying() async {
    _nowPage++;

    final Uri url = Uri.https(_url, '3/movie/now_playing', {
      'api_key': _apiKey,
      'language': _language,
      'page': _nowPage.toString(),
    });

    return await _processResponse(url);
  }

  Future<List<Movie>> getPopular() async {
    if (_loading) return [];

    _loading = true;
    _popularPage++;

    final Uri url = Uri.https(_url, '3/movie/popular', {
      'api_key': _apiKey,
      'language': _language,
      'page': _popularPage.toString(),
    });

    final response = await _processResponse(url);

    _popular.addAll(response);
    popularSink(_popular);

    _loading = false;

    return response;
  }

  Future<List<Actor>> getActors(String movieId) async {
    final Uri url = Uri.https(_url, '3/movie/$movieId/credits', {
      'api_key': _apiKey,
      'language': _language,
    });

    final response = await http.get(url);
    final decodedResponse = json.decode(response.body);
    final actors = Actors.fromJsonList(decodedResponse['cast']);

    return actors.items;
  }

  Future<List<Movie>> searchMovies(String query) async {
    final Uri url = Uri.https(_url, '3/search/movie', {
      'api_key': _apiKey,
      'language': _language,
      'query': query,
    });

    return await _processResponse(url);
  }
}
