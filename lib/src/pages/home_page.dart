import 'package:flutter/material.dart';
import 'package:movies/src/models/movie.dart';
import 'package:movies/src/providers/movies_providers.dart';
import 'package:movies/src/search/search_delegate.dart';
import 'package:movies/src/widgets/card_swiper.dart';
import 'package:movies/src/widgets/horizontal_movie.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('Películas'),
        backgroundColor: Colors.indigoAccent,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: DataSearch(),
              );
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _CardsSwiper(),
            _HomeFooter(),
          ],
        ),
      ),
    );
  }
}

class _HomeFooter extends StatelessWidget {
  _HomeFooter({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MoviesProvider moviesProvider = MoviesProvider();
    moviesProvider.getPopular();
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.0,
              vertical: 10.0,
            ),
            child: Text(
              'Populares',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          StreamBuilder<List<Movie>>(
            stream: moviesProvider.popularStream,
            builder: (_, AsyncSnapshot<List<Movie>> snapshot) {
              if (snapshot.hasData) {
                return HorizontalMovie(
                  movies: snapshot.data,
                  nextPage: moviesProvider.getPopular,
                );
              }

              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ],
      ),
    );
  }
}

class _CardsSwiper extends StatelessWidget {
  _CardsSwiper({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MoviesProvider moviesProvider = MoviesProvider();

    return FutureBuilder(
      future: moviesProvider.getMoviesNowPlaying(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return CardSwiper(
            movies: snapshot.data,
          );
        } else {
          return Container(
            height: MediaQuery.of(context).size.height / 2,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}
