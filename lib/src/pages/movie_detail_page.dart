import 'package:flutter/material.dart';
import 'package:movies/src/models/actor.dart';
import 'package:movies/src/models/movie.dart';
import 'package:movies/src/providers/movies_providers.dart';

class MovieDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Movie movie = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          _MovieDetailAppBar(movie: movie),
          SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(height: 20.0),
              _MoviePosterTitle(movie: movie),
              _MovieDescription(movie: movie),
              _MovieCasting(movie: movie),
            ]),
          ),
        ],
      ),
    );
  }
}

class _MovieDetailAppBar extends StatelessWidget {
  const _MovieDetailAppBar({
    Key key,
    @required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          movie.title,
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          ),
        ),
        background: movie.backdropPath == null
            ? null
            : FadeInImage(
                image: NetworkImage(movie.getBackdropImage()),
                placeholder: AssetImage('assets/images/jar-loading.gif'),
                fit: BoxFit.cover,
                fadeInDuration: Duration(milliseconds: 500),
              ),
      ),
    );
  }
}

class _MovieDescription extends StatelessWidget {
  const _MovieDescription({
    Key key,
    @required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 20.0,
        vertical: 20.0,
      ),
      child: Text(movie.overview),
    );
  }
}

class _MoviePosterTitle extends StatelessWidget {
  const _MoviePosterTitle({
    Key key,
    @required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: [
          Hero(
            tag: movie.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: NetworkImage(movie.getPosterImage()),
                height: 150.0,
              ),
            ),
          ),
          SizedBox(width: 20.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  movie.title,
                  style: Theme.of(context).textTheme.subtitle1,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  movie.originalTitle,
                  style: Theme.of(context).textTheme.caption,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 10.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.star_border, size: 30.0),
                    Text(
                      movie.voteAverage.toString(),
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _MovieCasting extends StatelessWidget {
  const _MovieCasting({
    Key key,
    @required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    final moviesProvider = MoviesProvider();

    return FutureBuilder(
      future: moviesProvider.getActors(movie.id.toString()),
      builder: (BuildContext context, AsyncSnapshot<List<Actor>> snapshot) {
        if (snapshot.hasData) {
          return _PageViewActors(actors: snapshot.data);
        } else {
          return Container(
            padding: EdgeInsets.all(20.0),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}

class _PageViewActors extends StatelessWidget {
  const _PageViewActors({
    Key key,
    @required this.actors,
  }) : super(key: key);

  final List<Actor> actors;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200.0,
      child: PageView.builder(
        controller: PageController(
          initialPage: 1,
          viewportFraction: 0.3,
        ),
        itemCount: actors.length,
        pageSnapping: false,
        itemBuilder: (BuildContext context, int index) {
          return ActorCard(actor: actors[index]);
        },
      ),
    );
  }
}

class ActorCard extends StatelessWidget {
  final Actor actor;

  ActorCard({this.actor});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: actor.profilePath != null
                ? _ActorImage(actor: actor)
                : _ImageNotFound(),
          ),
          SizedBox(height: 5.0),
          Text(
            actor.name,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}

class _ActorImage extends StatelessWidget {
  const _ActorImage({
    Key key,
    @required this.actor,
  }) : super(key: key);

  final Actor actor;

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      image: NetworkImage(actor.getProfileImage()),
      placeholder: AssetImage('assets/images/jar-loading.gif'),
      fit: BoxFit.cover,
      height: 150.0,
      width: 100.0,
    );
  }
}

class _ImageNotFound extends StatelessWidget {
  const _ImageNotFound({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(
      image: AssetImage('assets/images/no-image-found.png'),
      fit: BoxFit.cover,
      height: 150.0,
      width: 100.0,
    );
  }
}
