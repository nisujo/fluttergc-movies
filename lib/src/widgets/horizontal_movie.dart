import 'package:flutter/material.dart';
import 'package:movies/src/models/movie.dart';

class HorizontalMovie extends StatelessWidget {
  final List<Movie> movies;
  final Function nextPage;

  final PageController _pageController = PageController(
    initialPage: 1,
    viewportFraction: 0.3,
  );

  HorizontalMovie({
    @required this.movies,
    @required this.nextPage,
  });

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() {
      if (_pageController.position.pixels >=
          _pageController.position.maxScrollExtent - 200) {
        nextPage();
      }
    });

    return Container(
      height: screenSize.height * 0.27,
      child: PageView.builder(
        itemCount: movies.length,
        itemBuilder: (_, int index) => _MovieCard(movie: movies[index]),
        pageSnapping: false,
        controller: _pageController,
      ),
    );
  }
}

class _MovieCard extends StatelessWidget {
  const _MovieCard({
    Key key,
    @required this.movie,
  }) : super(key: key);

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    Widget image = Image(
      image: AssetImage('assets/images/no-image-found.png'),
      fit: BoxFit.cover,
      height: 120.0,
    );

    if (movie.posterPath != null) {
      image = FadeInImage(
        image: NetworkImage(movie.getPosterImage()),
        placeholder: AssetImage('assets/images/jar-loading.gif'),
        fit: BoxFit.cover,
        height: 120.0,
      );
    }

    movie.uniqueId = '${movie.id}-horizontal';

    Widget card = Container(
      margin: EdgeInsets.only(right: 10.0),
      child: Column(
        children: [
          Hero(
            tag: movie.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: image,
            ),
          ),
          SizedBox(height: 5.0),
          Text(
            movie.title,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );

    return GestureDetector(
      child: card,
      onTap: () => Navigator.of(context).pushNamed('detail', arguments: movie),
    );
  }
}
