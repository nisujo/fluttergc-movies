import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:movies/src/models/movie.dart';

class CardSwiper extends StatelessWidget {
  final List<Movie> movies;

  CardSwiper({@required this.movies});

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    double swiperHeight = _screenSize.height * 0.5;
    double itemWidth = _screenSize.width * 0.54;
    double itemHeight = swiperHeight;

    if (_screenSize.height < _screenSize.width) {
      swiperHeight = _screenSize.height * 0.75;
      itemWidth = _screenSize.width * 0.8;
      itemHeight = swiperHeight;
    }

    return Container(
      height: swiperHeight,
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          Movie movie = movies[index];
          Widget image = Image(
            image: AssetImage('assets/images/no-image-found.png'),
            fit: BoxFit.cover,
            height: 120.0,
          );

          if (movie.posterPath != null) {
            image = FadeInImage(
              image: NetworkImage(movie.getPosterImage()),
              fit: BoxFit.cover,
              placeholder: AssetImage('assets/images/jar-loading.gif'),
            );
          }

          movie.uniqueId = '${movie.id}-big';

          return Container(
            child: Hero(
              tag: movie.uniqueId,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: GestureDetector(
                  child: image,
                  onTap: () => Navigator.of(context)
                      .pushNamed('detail', arguments: movie),
                ),
              ),
            ),
          );
        },
        itemCount: movies.length,
        itemWidth: itemWidth,
        itemHeight: itemHeight,
        layout: SwiperLayout.STACK,
        control: SwiperControl(),
      ),
    );
  }
}
